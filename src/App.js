import React from 'react';
import Buttons from './pages/Buttons.jsx';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar/Navbar';

const App = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path='/buttons' element={<Buttons />} />
      </Routes>
    </BrowserRouter>
  );
};

export default App;
