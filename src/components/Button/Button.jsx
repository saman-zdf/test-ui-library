import React from 'react';
import styles from './Buttons.module.scss';
const Button = ({
  onClick,
  type = 'button',
  size,
  color,
  variants,
  children,
  disabled,
  ...props
}) => {
  const buttonStyle = [styles.Button];
  if (size) {
    buttonStyle.push(styles[size]);
  }
  if (variants) {
    buttonStyle.push(styles[variants]);
  }
  return (
    <button
      type={type}
      onClick={onClick}
      className={buttonStyle.join(' ')}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

export default Button;
