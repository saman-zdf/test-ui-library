import React from 'react';
import Button from '../components/Button/Button';
import { BUTTON_VARIANTS } from '../utils/UIButtonStyles';
import styles from './Buttons.module.scss';
import { TiArrowSync } from 'react-icons/ti';
const Home = () => {
  return (
    <div className={styles.main}>
      <h1 className={styles.heading}>Buttons</h1>
      <Button variants={BUTTON_VARIANTS.PRIMARY}>Primary</Button>
      <Button variants={BUTTON_VARIANTS.SECONDARY}>Secondary</Button>
      <Button variants={BUTTON_VARIANTS.ACTIONED}>Actioned</Button>
      <Button disabled variants={BUTTON_VARIANTS.DISABLED}>
        Disabled
      </Button>
      <Button disabled variants={BUTTON_VARIANTS.DISABLED}>
        <TiArrowSync />
        {'  '}
        Disabled with icon
      </Button>
    </div>
  );
};

export default Home;
