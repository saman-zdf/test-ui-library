export const BUTTON_SIZES = {
  REGULAR: 'regular',
  LARGE: 'large',
  SMALL: 'small',
  EXTRA_SMALL: 'extra_small',
};

export const BUTTON_VARIANTS = {
  PRIMARY: 'primary',
  SECONDARY: 'secondary',

  ACTIONED: 'actioned',

  DISABLED: 'disabled',
};
